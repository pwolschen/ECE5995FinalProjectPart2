load  MNIST_data
load W


% Break the set into training images and validation images
idx = randperm(size(images_train,2));
for i=1:size(images_train,2)
    images(:,i) = images_train(:,idx(i));
    labels(i) = labels_train(idx(i));
end

trainingSetImages = images(:,1:5000);
trainingSetLabels = labels(1:5000)';
valSetImages = images(:,50001:60000);
valSetLabels = labels(50001:60000)';

arrayOfOnes = ones(size(valSetImages,2),1);
valSetImages = [valSetImages; arrayOfOnes'];

Classifier(valSetImages,valSetLabels,W1,W2,W3,300,100,10);

