function Misclassified(misclassifiedIndices,images,labels,W1,W2,W3,H,J,L)
%MISCLASSIFIED Plot the misclassified values

size(misclassifiedIndices,2)
for i=1:size(misclassifiedIndices,2)
    index = misclassifiedIndices(i);
    image(:) = images(:,index);
    label = labels(index);


    [classification,probability,allProps] = ForwardProp(image',W1,W2,W3,H,J,L);
    figure(1)
    bar([0:9],allProps)
    title({['Classified as ',num2str(classification)];['The label is ', num2str(label)]})
    fileName = sprintf('pic%d_bar.png', i);
    saveas(gcf,fileName)
    figure(2)
    DrawNumber(image)
    fileName = sprintf('pic%d.png', i);
    saveas(gcf,fileName)
end

