function [W1,W2,W3] = TrainNeuralNet(trainingSetImages,trainingSetLabels,valSetImages,valSetLabels, H, J, L, ro, rh, rh2)
%TRAINNEURALNET Train the neural net based on the images

% Add 1 bias
arrayOfOnes = ones(size(trainingSetLabels,1),1);
trainingSetImages = [trainingSetImages; arrayOfOnes'];
arrayOfOnes = ones(size(valSetImages,2),1);
valSetImages = [valSetImages; arrayOfOnes'];

% Create Targets
for i=1:size(trainingSetLabels,1)
    % Create array of all -1s
    targetSet(i,:) = -1*ones(1,L);
    % Set the target index to 1
    targetSet(i,trainingSetLabels(i)+1) = 1;
end

n = size(trainingSetImages,1);

W1 = (rand(H,n)-.5)*.2;
W2 = (rand(J,H+1)-.5)*.2;
W3 = (rand(L,J+1)-.5)*.2;
errorRate = 0;
bestepoch = 0;

for epoch=1:400
    % Train for 1 epoch
    [W1,W2,W3] = IterateTraining(trainingSetImages, targetSet, W1, W2, W3, H, J, L,rh,rh2,ro);
   
    % Check error rate
    hit = 0;
    missedIndices = [];
    missedIndicesTrain = [];
    for i=1:size(valSetLabels,1)
        image = valSetImages(:,i);
        target = valSetLabels(i);
        output = ForwardProp(image,W1,W2,W3,H,J,L);
        if target == output
            hit = hit + 1;
        else
            missedIndices = [missedIndices,i];
        end
    end
    errorRateVal(epoch) = (size(valSetLabels,1)-hit)/size(valSetLabels,1) 
    hit = 0;
    for i=1:size(trainingSetLabels,1)
        image = trainingSetImages(:,i);
        target = trainingSetLabels(i);
        output = ForwardProp(image,W1,W2,W3,H,J,L);
        if target == output
            hit = hit + 1;
        else
            missedIndicesTrain = [missedIndices,i];
        end
    end
    
    errorRateTrain(epoch) = (size(trainingSetLabels,1)-hit)/size(trainingSetLabels,1);
    
    %plot data
    clf
    plot(errorRateVal,'b')
    hold on
    plot(errorRateTrain,'r')
    title({['Training and Validation Error'];['J=', num2str(J), ',H=', num2str(H)];[',ro=',num2str(ro),',rh=',num2str(rh),',rh2=',num2str(rh2)]});
    legend('Validation Error', 'Training Error');
    
    % Find the best point
    if epoch > 1 && errorRateVal(epoch) < min(errorRateVal(1:epoch-1))
        bestW1 = W1;
        bestW2 = W2;
        bestW3 = W3;
        bestepoch = epoch;
        bestErrorRate = errorRateVal(epoch);
    end
    
    if epoch - bestepoch > 25 || (epoch > 25 && bestErrorRate + (bestErrorRate*.05) < errorRateVal(epoch))
        break;
    end
end

% Save for data collection and review
figure(1)
pause(1)
fileName = sprintf("%d_%d_%f_%f_%f.png",H,J,ro,rh,rh2);
saveas(gcf,fileName);
fileName = sprintf("%d_%d_%f_%f_%f.mat",H,J,ro,rh,rh2);
save(fileName,'bestW1', 'bestW2', 'bestW3', 'bestepoch', 'bestErrorRate', 'errorRateVal', 'errorRateTrain');


end





