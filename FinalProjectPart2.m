load  MNIST_data
%load MNIST_data_1000train_500test

% Break the set into training images and validation images
idx = randperm(size(images_train,2));
for i=1:size(images_train,2)
    images(:,i) = images_train(:,idx(i));
    labels(i) = labels_train(idx(i));
end

trainingSetImages = images(:,1:5000);
trainingSetLabels = labels(1:5000)';
valSetImages = images(:,50001:60000);
valSetLabels = labels(50001:60000)';
% trainingSetImages = images;
% trainingSetLabels = labels';
% valSetImages = images_test;
% valSetLabels = labels_test;

% If weights exist, don't re-learn
if exist('W1','var') ~= 1    
    [W1,W2,W3] = TrainNeuralNet(trainingSetImages,trainingSetLabels,valSetImages,valSetLabels,150,300,10,.0001,.0001,.0001);
end
    
Retrieval
