function [W1,W2,W3] = IterateTraining(trainingSetImages,trainingSetLabels, W1, W2, W3,H,J,L,rh,rh2,ro)
%ITERATETRAINING Go through 1 epoch of training

m = size(trainingSetImages,2);
n = size(trainingSetImages,1);

fh = @(x) tanh(x);
fh2 = @(x) tanh(x);
fo = @(x) tanh(x);
dfh = @(x) 1-tanh(x)^2;
dfh2 = @(x) 1-tanh(x)^2;
dfo = @(x) 1-tanh(x)^2;

for iteration=1:m
    index = iteration;
    x = trainingSetImages(:,index);
    d = trainingSetLabels(index,:)';
    z = ones(J,1);
    a = ones(H,1);
    neth = ones(H,1);
    netj = ones(J,1);
    netl = ones(L,1);
    
    
    neth = W1*x;
    a = tanh(neth);
    a = [1;a];

    netj = W2*a;
    z = tanh(netj);
    
    z = [1;z];

    
    netl = W3*z;
    y = tanh(netl);
    
    
    propErrorj = zeros(J,1);
    propErrorj2 = zeros(1,J+1);

    for l=1:L
        dW3(l,:) = ro*(d(l)-y(l))*(1-tanh(netl(l))^2)*z;
        propErrorj2 = propErrorj2 + (d(l)-y(l))*(1-tanh(netl(l))^2)*W3(l,:);
    end
    propErrorj = propErrorj2';

    propErrorh = zeros(H,1);
    propErrorh2 = zeros(1,H+1);

    for j=1:J
        dW2(j,:) = rh2*(propErrorj(j))*(1-tanh(netj(j))^2)*a;
        propErrorh2 =  propErrorh2 + (propErrorj(j)).*W2(j,:);        
    end
    propErrorh = propErrorh2';
   
    for h=1:H
        dW1(h,:) = rh*(propErrorh(h))*(1-tanh(neth(h))^2)*x;
    end

    
    W1 = W1 + dW1;
    W2 = W2 + dW2;
    W3 = W3 + dW3;
end
        



end

