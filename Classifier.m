function errorRate = Classifier(images,labels,W1,W2,W3,H,J,L)
%CLASSIFIER Find the error rate
    hit = 0;
     missedIndices = [];
    for i=1:size(labels,1)
        image = images(:,i);
        target = labels(i);
        output = ForwardProp(image,W1,W2,W3,H,J,L);
        if target == output
            hit = hit + 1;
        else
            missedIndices = [missedIndices,i];
        end
    end
    errorRate = (size(labels,1)-hit)/size(labels,1) ;
    Misclassified(missedIndices, images,labels, W1, W2, W3,H,J,L);
end

