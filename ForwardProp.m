function [result, val,vals] = ForwardProp(image,W1,W2,W3,H,J,L)
%FORWARDPROP Calculate the output of the neural net by forward propagating
%the values
fh = @(x) tanh(x);
fh2 = @(x) tanh(x);
fo = @(x) tanh(x);
dfh = @(x) 1-tanh(x)^2;
dfh2 = @(x) 1-tanh(x)^2;
dfo = @(x) 1-tanh(x)^2;


    neth = (W1*image);
    a = fh(neth);
    
    a = [1;a];

    netj = (W2*a);
    z = fh2(netj);
    z = [1;z];

    netl = (W3*z);
    y = fo(netl);
    
[val,idx] = max(y);
result = idx-1;
vals = y;



end

