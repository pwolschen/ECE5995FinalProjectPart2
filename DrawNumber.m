function DrawNumber(imageArray)
%DRAWNUMBER Summary of this function goes here
%   Detailed explanation goes here

%The image is a 28x28 array represented as 784
%the input is an array of these values
image = zeros(28,28);
row = 1;
column = 1;

%Seperate into a 2 dimensional image
for i = 1:784
    ArrayVal = imageArray(i);
    image(row,column) = imageArray(i);
    if row >= 28
        row = 0;
        column = column + 1;
    end
    if column == 28
        column = 1;
    end        
    
    row = row + 1;
end

%image
%imagesc(image)
imshow(imresize(image,20));



